#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char* argv[]){
	int i, nbPass;
	const char tempFilename[] = "tempEncrypted";
	double totTimeEnc = 0, totTimeDec = 0;
	double tempTime = 0;
	
	clock_t startTimMes, endTimMes;

	if(argc == 3){
		nbPass = 1;	//Default: One pass in encrypt-decrypt
	}else if(argc == 4){
		nbPass = atoi(argv[3]);
	}else{
		printf("ERROR: Incorrect number of args\n");
		return(1);	//Incorrect number of arguments
	}

	char strEnc[100], strDec[100];
	strcpy(strEnc, "./encMes ");
	strcat(strEnc, argv[1]);
	strcat(strEnc, " ");
	strcat(strEnc, tempFilename);
	
	strcpy(strDec, "./decrypt ");
	strcat(strDec, tempFilename);
	strcat(strDec, " ");
	strcat(strDec, argv[2]);
	
	for(i=0; i<nbPass; i++){
		startTimMes = clock();
		system(strEnc);
		endTimMes = clock();
		tempTime = (double)(endTimMes - startTimMes) / CLOCKS_PER_SEC;	//Time spent on encryption
		totTimeEnc += tempTime;
		printf("Encryption, pass N°%d: %fms\n", i+1, tempTime*1000);
		
		startTimMes = clock();
		system(strDec); //Calls decryption program
		endTimMes = clock();
		tempTime = (double)(endTimMes - startTimMes) / CLOCKS_PER_SEC;	//Time spent on encryption
		totTimeDec += tempTime;
		printf("Decryption, pass N°%d: %fms\n", i+1, tempTime*1000);
	}

	printf("\nAverage times:\n-----------------------------\nEncryption: %fms\nDecryption: %fms\n", totTimeEnc/i*1000, totTimeDec/i*1000);

	return(0);
}

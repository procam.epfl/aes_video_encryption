#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
	
	if(argc != 3){
		printf("Wrong number of arguments\n");
		return(1);
	}
	
	int size = atoi(argv[2]);	//Number of kilobytes
	FILE* f = fopen(argv[1],"w"); //Random file to create
	
	int i,j;
	
	for(i=0; i<size; i++){
		for(j=0; j<1024; j++){	//1024 Bytes per kB (no use testing under kB scale)
			putc((char)(rand() % 256), f);	//Random 8b into char
		}
	}
	
	fclose(f);
	
	return(0);
}

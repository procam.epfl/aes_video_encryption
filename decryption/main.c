#include <stdio.h>
#include <stdlib.h>

#include "aes.h"

#define NBBYTES_TO_ENCRYPT 256*AES_BLOCKLEN

int main(int argc, char* argv[]){
	int i;
	uint8_t buffer[NBBYTES_TO_ENCRYPT];
	uint8_t initVector[AES_BLOCKLEN];
	size_t nbElementsRead;

	struct AES_ctx ctx;
	const uint8_t key[AES_BLOCKLEN] = {0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5};	//EXAMPLE KEY! TO CHANGE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	if(argc != 3){
		printf("ERROR: Incorrect number of args\n");
		return(1);	//Incorrect number of arguments
	}

	FILE* inputFile = fopen(argv[1],"r"); //Read-only input file
	FILE* outputFile = fopen(argv[2],"w");	//Write-only output file

	if(inputFile == NULL || outputFile == NULL){
		printf("ERROR: Invalid file names\n");
		return(2);
	}

	//Initialization vector fetching
	fread(initVector, 1, AES_BLOCKLEN, inputFile);

	AES_init_ctx_iv(&ctx, key, initVector);	//Initializes encryption structure

	nbElementsRead = fread(buffer, 1, NBBYTES_TO_ENCRYPT, inputFile);	//First check to ensure long enough file
	while(nbElementsRead == NBBYTES_TO_ENCRYPT){	//While still enough numbers in file
		AES_CBC_decrypt_buffer(&ctx, buffer, NBBYTES_TO_ENCRYPT);
		fwrite(buffer, 1, NBBYTES_TO_ENCRYPT, outputFile);
		nbElementsRead = fread(buffer, 1, NBBYTES_TO_ENCRYPT, inputFile);
	}

	if(nbElementsRead != 0){	//Pads if still bits left
		for(i=nbElementsRead; i<NBBYTES_TO_ENCRYPT; i++){
			buffer[i] = 0;	//Uses 0s for padding
		}

		//Last AES call
		AES_CBC_decrypt_buffer(&ctx, buffer, NBBYTES_TO_ENCRYPT);
		fwrite(buffer, 1, NBBYTES_TO_ENCRYPT, outputFile);
	}

	fclose(inputFile);
	fclose(outputFile);

	return(0);
}

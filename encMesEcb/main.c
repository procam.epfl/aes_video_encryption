#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "aes.h"


int main(int argc, char* argv[]){
	int i,j, tempTime;
	uint8_t buffer[AES_BLOCKLEN];
	size_t nbElementsRead;

	struct AES_ctx ctx;
	const uint8_t key[AES_BLOCKLEN] = {0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5};	//EXAMPLE KEY! TO CHANGE!

	int start,end;

	start = clock();

	if(argc != 4){
		printf("ERROR: Incorrect number of args\n");
		return(1);	//Incorrect number of arguments
	}
for(j = 0; j < atoi(argv[3]); j++){

	FILE* inputFile = fopen(argv[1],"r"); //Read-only input file
	FILE* outputFile = fopen(argv[2],"w");	//Write-only output file

	if(inputFile == NULL || outputFile == NULL){
		printf("ERROR: Invalid file names\n");
		return(2);
	}

	nbElementsRead = fread(buffer, 1, AES_BLOCKLEN, inputFile);	//First check to ensure long enough file
	while(nbElementsRead == AES_BLOCKLEN){	//While still enough numbers in file
		AES_ECB_encrypt(&ctx, buffer);
		fwrite(buffer, 1, AES_BLOCKLEN, outputFile);
		nbElementsRead = fread(buffer, 1, AES_BLOCKLEN, inputFile);
	}

	if(nbElementsRead != 0){	//Pads if still bits left
		for(i=nbElementsRead; i<AES_BLOCKLEN; i++){
			buffer[i] = 0;	//Uses 0s for padding
		}

		//Last AES call
		AES_ECB_encrypt(&ctx, buffer);
		fwrite(buffer, 1, AES_BLOCKLEN, outputFile);
	}

	fclose(inputFile);
	fclose(outputFile);
}
	end = clock();

	tempTime = (double)(end - start) / CLOCKS_PER_SEC;
	printf("end %d, start %d\n%f\n", end, start, (double)(end-start)/CLOCKS_PER_SEC);

	return(0);
}
